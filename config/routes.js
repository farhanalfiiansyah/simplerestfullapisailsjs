/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  // t_registrasi
  '/': { view: 'pages/homepage' },
  'POST /registrasi' : 'RegistrasiController.create',
  'GET /registrasi' : 'RegistrasiController.find',
  'GET /registrasi/:id' : 'RegistrasiController.findOne',
  'PUT /registrasi' : 'RegistrasiController.update',
  'DELETE /registrasi' : 'RegistrasiController.delete',

  // m_jenis_unit_asal
  'POST /jenis-unit-asal' : 'JenisUnitAsalController.create',
  'GET /jenis-unit-asal' : 'JenisUnitAsalController.find',
  'GET /jenis-unit-asal/:id' : 'JenisUnitAsalController.findOne',
  'PUT /jenis-unit-asal' : 'JenisUnitAsalController.update',
  'DELETE /jenis-unit-asal' : 'JenisUnitAsalController.delete',

  // m_flag
  'POST /flag' : 'FlagController.create',
  'GET /flag' : 'FlagController.find',
  'GET /flag/:id' : 'FlagController.findOne',
  'PUT /flag' : 'FlagController.update',
  'DELETE /flag' : 'FlagController.delete',

  // m_group_flag_angka
  'POST /group-flag-angka' : 'GroupFlagAngkaController.create',
  'GET /group-flag-angka' : 'GroupFlagAngkaController.find',
  'GET /group-flag-angka/:id' : 'GroupFlagAngkaController.findOne',
  'PUT /group-flag-angka' : 'GroupFlagAngkaController.update',
  'DELETE /group-flag-angka' : 'GroupFlagAngkaController.delete',

  // m_kategori_pemeriksaan
  'POST /kategori-pemeriksaan' : 'KategoriPemeriksaanController.create',
  'GET /kategori-pemeriksaan' : 'KategoriPemeriksaanController.find',
  'GET /kategori-pemeriksaan/:id' : 'KategoriPemeriksaanController.findOne',
  'PUT /kategori-pemeriksaan' : 'KategoriPemeriksaanController.update',
  'DELETE /kategori-pemeriksaan' : 'KategoriPemeriksaanController.delete',

  // m_group_flag_text
  'POST /group-flag-text' : 'GroupFlagTextController.create',
  'GET /group-flag-text' : 'GroupFlagTextController.find',
  'GET /group-flag-text/:id' : 'GroupFlagTextController.findOne',
  'PUT /group-flag-text' : 'GroupFlagTextController.update',
  'DELETE /group-flag-text' : 'GroupFlagTextController.delete',

  // m_paket_pemeriksaan
  'POST /paket-pemeriksaan' : 'PaketPemeriksaanController.create',
  'GET /paket-pemeriksaan' : 'PaketPemeriksaanController.find',
  'GET /paket-pemeriksaan/:id' : 'PaketPemeriksaanController.findOne',
  'PUT /paket-pemeriksaan' : 'PaketPemeriksaanController.update',
  'DELETE /paket-pemeriksaan' : 'PaketPemeriksaanController.delete',

  // m_pilihan_hasil
  'POST /pilihan-hasil' : 'PilihanHasilController.create',
  'GET /pilihan-hasil' : 'PilihanHasilController.find',
  'GET /pilihan-hasil/:id' : 'PilihanHasilController.findOne',
  'PUT /pilihan-hasil' : 'PilihanHasilController.update',
  'DELETE /pilihan-hasil' : 'PilihanHasilController.delete',

  // m_mapping_tindakan_sim_rs
  'POST /mapping-tindakan-sim-rs' : 'MappingTindakanSimRSController.create',
  'GET /mapping-tindakan-sim-rs' : 'MappingTindakanSimRSController.find',
  'GET /mapping-tindakan-sim-rs/:id' : 'MappingTindakanSimRSController.findOne',
  'PUT /mapping-tindakan-sim-rs' : 'MappingTindakanSimRSController.update',
  'DELETE /mapping-tindakan-sim-rs' : 'MappingTindakanSimRSController.delete',

  // m_sub_kategori_pemeriksaan
  'POST /sub-kategori-pemeriksaan' : 'SubKategoriPemeriksaanController.create',
  'GET /sub-kategori-pemeriksaan' : 'SubKategoriPemeriksaanController.find',
  'GET /sub-kategori-pemeriksaan/:id' : 'SubKategoriPemeriksaanController.findOne',
  'PUT /sub-kategori-pemeriksaan' : 'SubKategoriPemeriksaanController.update',
  'DELETE /sub-kategori-pemeriksaan' : 'SubKategoriPemeriksaanController.delete',

  // r_paket_pemeriksaan
  'POST /r-paket-pemeriksaan' : 'RPaketPemeriksaanController.create',
  'GET /r-paket-pemeriksaan' : 'RPaketPemeriksaanController.find',
  'GET /r-paket-pemeriksaan/:id' : 'RPaketPemeriksaanController.findOne',
  'PUT /r-paket-pemeriksaan' : 'RPaketPemeriksaanController.update',
  'DELETE /r-paket-pemeriksaan' : 'RPaketPemeriksaanController.delete',

  // m_nilai_rujukan
  'POST /nilai-rujukan' : 'NilaiRujukanController.create',
  'GET /nilai-rujukan' : 'NilaiRujukanController.find',
  'GET /nilai-rujukan/:id' : 'NilaiRujukanController.findOne',
  'PUT /nilai-rujukan' : 'NilaiRujukanController.update',
  'DELETE /nilai-rujukan' : 'NilaiRujukanController.delete',

  // m_item_pemeriksaan
  'POST /item-pemeriksaan' : 'ItemPemeriksaanController.create',
  'GET /item-pemeriksaan' : 'ItemPemeriksaanController.find',
  'GET /item-pemeriksaan/:id' : 'ItemPemeriksaanController.findOne',
  'PUT /item-pemeriksaan' : 'ItemPemeriksaanController.update',
  'DELETE /item-pemeriksaan' : 'ItemPemeriksaanController.delete',

  // m_unit_asal
  'POST /unit-asal' : 'UnitAsalController.create',
  'GET /unit-asal' : 'UnitAsalController.find',
  'PUT /unit-asal' : 'UnitAsalController.update',
  'GET /unit-asal/:id' : 'UnitAsalController.findOne',
  'DELETE /unit-asal' : 'UnitAsalController.delete'
  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
