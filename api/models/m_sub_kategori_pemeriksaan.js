/**
 * M_sub_kategori_pemeriksaan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const m_item_pemeriksaan = require("./m_item_pemeriksaan");

module.exports = {

  attributes: {

    nama : {
      type : 'string'
    },
    no_urut : {
      type : 'number'
    },
    m_kategori_pemeriksaan_id : {
      model : 'm_kategori_pemeriksaan',
      required : true
    },
    kode : {
      type : 'string'
    },
    status : {
      type : 'number',
      min : 0,
      max : 1
    },
    m_item_pemeriksaan : {
      collection : 'm_item_pemeriksaan',
      via : 'm_sub_kategori_pemeriksaan_id'
    }

  },

};

