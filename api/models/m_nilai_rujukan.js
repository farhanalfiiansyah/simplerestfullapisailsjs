/**
 * M_nilai_rujukan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const m_group_flag_angka = require("./m_group_flag_angka");
const m_group_flag_text = require("./m_group_flag_text");

module.exports = {

  attributes: {

    tampilan_nilai_rujukan : {
      type : 'string'
    },
    jenis_rule : {
      type : 'string'
    },
    m_item_pemeriksaan_id : {
      model : 'm_item_pemeriksaan',
      required : true
    },
    rule_batas_umur_atas_tahun : {
      type : 'number'
    },
    rule_batas_umur_atas_bulan : {
      type : 'number'
    },
    rule_batas_umur_atas_hari : {
      type : 'number'
    },
    rule_batas_umur_bawah_tahun : {
      type : 'number'
    },
    rule_batas_umur_bawah_bulan : {
      type : 'number'
    },
    rule_batas_umur_bawah_hari : {
      type : 'number'
    },
    rule_jenis_kelamin : {
      type : 'string'
    },
    rule_nilai_angka_rujukan_bawah : {
      type : 'ref',
      columnType : 'double',
    },
    rule_nilai_angka_rujukan_atas : {
      type : 'ref',
      columnType : 'double'
    },
    rule_nilai_angka_kritis_atas : {
      type : 'ref',
      columnType : 'double'
    },
    rule_nilai_angka_kritis_bawah : {
      type : 'ref',
      columnType : 'double'
    },
    rule_nilai_text_nilai_rujukan : {
      type : 'json'
    },
    m_group_flag_angka_id : {
      model : 'm_group_flag_angka',
      required : true
    },
    m_group_flag_text_id : {
      model : 'm_group_flag_text',
      required : true
    }

  },

};

