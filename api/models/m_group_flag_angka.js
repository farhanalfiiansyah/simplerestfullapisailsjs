/**
 * M_group_flag_angka.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    kode : {
      type : 'string'
    },
    m_flag_id_nilai_rujukan_bawah : {
      model : 'm_flag',
      required : true
    },
    m_flag_id_nilai_rujukan_atas : {
      model : 'm_flag',
      required : true
    },
    m_flag_id_normal : {
      model : 'm_flag',
      required : true
    },
    m_flag_id_nilai_kritis_bawah : {
      model : 'm_flag',
      required : true
    },
    m_flag_id_nilai_kritis_atas : {
      model : 'm_flag',
      required : true
    },
    m_nilai_rujukan : {
      collection : 'm_nilai_rujukan',
      via : 'm_group_flag_angka_id'
    }

  },

};

