/**
 * M_unit_asal.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nama : {
      type : 'string',
      required : true
    },
    kode : {
      type : 'string',
      required : true
    },
    kelas : {
      type : 'string',
      required : true
    },
    status : {
      type : 'number',
      min : 0,
      max : 1,
      required : true
    },
    keterangan : {
      type : 'string',      
      required : true
    },
    m_jenis_unit_asal_id : {
      model : 'm_jenis_unit_asal',
      required : true
    }
  },
};

