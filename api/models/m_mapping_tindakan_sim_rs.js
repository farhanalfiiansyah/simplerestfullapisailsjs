/**
 * M_mapping_tindakan_sim_rs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    kode : {
      type : 'string'
    },
    jenis_bridging : {
      type : 'string'
    },
    nama : {
      type : 'string'
    },
    m_paket_pemeriksaan_id : {
      model : 'm_paket_pemeriksaan',
      required : true
    },
    m_item_pemeriksaan_id : {
      model : 'm_item_pemeriksaan',
      required : true
    },
    status : {
      type : 'number',
      max : 1,
      min : 0
    }

  },

};

