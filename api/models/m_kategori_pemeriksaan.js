/**
 * M_kategori_pemeriksaan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    nama : {
      type : 'string'
    },
    no_urut : {
      type : 'number'
    },
    kode : {
      type : 'string'
    },
    status : {
      type : 'number',
      max : 1,
      min : 0
    },
    m_item_pemeriksaan : {
      collection : 'm_item_pemeriksaan',
      via : 'm_kategori_pemeriksaan_id'
    },
    m_sub_kategori_pemeriksaan : {
      collection : 'm_sub_kategori_pemeriksaan',
      via : 'm_kategori_pemeriksaan_id'
    }
  },

};

