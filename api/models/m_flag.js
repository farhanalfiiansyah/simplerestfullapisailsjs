/**
 * M_flag.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    kode: {
      type : 'string',
    },
    nama: {
      type : 'string'
    },
    warna: {
      type : 'string'
    },
    jenis_pewarnaan: {
      type : 'string'
    },
    status: {
      type : 'number',
      max : 1,
      min : 0
    },
    m_group_flag_angka_nilai_rujukan_bawah : {
      collection : 'm_group_flag_angka',
      via : 'm_flag_id_nilai_rujukan_bawah'
    },
    m_group_flag_angka_nilai_rujukan_atas : {
      collection : 'm_group_flag_angka',
      via : 'm_flag_id_nilai_rujukan_atas'
    },
    m_group_flag_angka_normal : {
      collection : 'm_group_flag_angka',
      via : 'm_flag_id_normal'
    },
    m_group_flag_angka_nilai_kritis_bawah : {
      collection : 'm_group_flag_angka',
      via : 'm_flag_id_nilai_kritis_bawah'
    },
    m_group_flag_angka_nilai_kritis_atas : {
      collection : 'm_group_flag_angka',
      via : 'm_flag_id_nilai_kritis_atas'
    },
    m_group_flag_text_normal : {
      collection : 'm_group_flag_text',
      via : 'm_flag_id_normal'
    },
    m_group_flag_text_tidak_normal : {
      collection : 'm_group_flag_text',
      via : 'm_flag_id_tidak_normal'
    }
  },

};

