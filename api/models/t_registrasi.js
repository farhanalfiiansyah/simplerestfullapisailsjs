/**
 * T_registrasi.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: {
      type: 'number',
      autoIncrement: true,
      required : true
    },
    no_lab: {
      type:'string',      
      required : true
    },
    no_reg_rs: {
      type: 'string',
      required : true
    },
    diagnosa_awal: {
      type: 'string',
      required : true
    },
    keterangan_klinis: {
      type: 'string',
      required : true
    },
    keterangan_hasil: {
      type: 'string',
      required : true
    },
    expertise: {
      type: 'string',
      required : true
    },
    waktu_expertise: {
      type: 'ref',
      columnType: 'datetime'
    },
    status_terbit: {
      type: 'number',
      required : true
    },
    waktu_terbit: {
      type: 'ref',
      columnType: 'datetime'
    },
    status_registrasi: {
      type: 'number',
      required : true
    },
    waktu_registrasi: {
      type: 'ref',
      columnType: 'datetime'
    },
    total_bayar: {
      type: 'number',
      required : true
    },
    total_print: {
      type: 'number',
      required : true
    },
    pasien_no_rm: {
      type: 'string',
      required : true
    },
    pasien_nama: {
      type: 'string',
      required : true
    },
    pasien_jenis_kelamin: {
      type: 'string',
      required : true
    },
    pasien_tanggal_lahir:{
      type: 'ref',
      columnType: 'date'
    },
    pasien_alamat: {
      type: 'string',
      required : true
    },
    pasien_no_telphone: {
      type: 'string',
      allowNull: true
    },
    pasien_umur_hari: {
      type: 'number',
      allowNull: true
    },
    pasien_umur_bulan: {
      type: 'number',
      allowNull: true
    },
    pasien_umur_tahun: {
      type: 'number',
      allowNull: true
    },
    dokter_pengirim_nama: {
      type: 'string',
      allowNull: true
    },
    dokter_pengirim_kode: {
      type: 'string',
      allowNull: true
    },
    dokter_pengirim_alamat: {
      type: 'string',
      allowNull: true
    },
    dokter_pengirim_no_telphone: {
      type: 'string',
      allowNull: true
    },
    dokter_pengirim_spesialis_nama: {
      type: 'string',
      allowNull: true
    },
    dokter_pengirim_spesialis_kode: {
      type: 'string',
      allowNull: true
    },
    unit_asal_nama: {
      type: 'string',
      allowNull: true
    },
    unit_asal_kode: {
      type: 'string',
      allowNull: true
    },
    unit_asal_kelas: {
      type: 'string',
      allowNull: true
    },
    unit_asal_keterangan: {
      type: 'string',
      allowNull: true
    },
    unit_asal_jenis_nama: {
      type: 'string',
      allowNull: true
    },
    penjamin_nama: {
      type: 'string',
      allowNull: true
    },
    penjamin_kode: {
      type: 'string',
      allowNull: true
    },
    penjamin_jenis_nama: {
      type: 'string',
      allowNull: true
    },
    penjamin_jenis_kode: {
      type: 'string',
      allowNull: true
    },
    created: {
      type: 'ref',
      columnType: 'datetime'
    },
    updated: {
      type: 'ref',
      columnType: 'datetime'
    }
  },

};

