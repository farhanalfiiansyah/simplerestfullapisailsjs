/**
 * M_paket_pemeriksaan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    kode : {
      type : 'string'
    },
    nama : {
      type : 'string'
    },
    status : {
      type : 'number',
      max : 1,
      min : 0
    },
    harga : {
      type : 'number'
    },
    m_r_paket_pemeriksaan : {
      collection : 'm_r_paket_pemeriksaan',
      via : 'm_paket_pemeriksaan_id'
    },
    m_mapping_tindakan_sim_rs : {
      collection : 'm_mapping_tindakan_sim_rs',
      via : 'm_paket_pemeriksaan_id'
    }

  },

};

