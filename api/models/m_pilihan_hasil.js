/**
 * M_pilihan_hasil.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    nilai : {
      type : 'json'
    },
    m_item_pemeriksaan_id : {
      model : 'm_item_pemeriksaan',
      required : true
    }

  },

};

