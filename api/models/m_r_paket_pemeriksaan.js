/**
 * M_r_paket_pemeriksaan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    m_item_pemeriksaan_id : {
      model : 'm_item_pemeriksaan',
      required : true
    },
    m_paket_pemeriksaan_id : {
      model : 'm_paket_pemeriksaan',
      required : true
    }

  },

};

