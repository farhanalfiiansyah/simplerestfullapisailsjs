/**
 * M_jenis_unit_asal.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nama : {
      type : 'string',
      required : true
    },
    kode : {
      type : 'string',
      required : true
    },
    status : {
      type : 'number',
      min : 0,
      max : 1,
      required : true
    },
    m_unit_asal : {
      collection : 'm_unit_asal',
      via : 'm_jenis_unit_asal_id'
    },
  },

};

