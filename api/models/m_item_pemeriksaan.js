/**
 * M_item_pemeriksaan.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */


module.exports = {

  attributes: {

    kode : {
      type : 'string'
    },
    nama : {
      type : 'string'
    },
    satuan : {
      type : 'string'
    },
    metode : {
      type : 'string'
    },
    no_urut : {
      type : 'number'
    },
    jenis_input : {
      type : 'string'
    },
    format_hasil_cetak : {
      type : 'string'
    },
    is_tampilkan_waktu_periksa : {
      type : 'number',
      min : 0,
      max : 1
    },
    is_flag_nilai_rujukan : {
      type : 'number',
      min : 0,
      max : 1
    },
    is_flag_nilai_kritis : {
      type : 'number',
      min : 0,
      max : 1
    },
    harga : {
      type : 'number'
    },
    status : {
      type : 'number',
      min : 0,
      max : 1
    },
    m_kategori_pemeriksaan_id : {
      model : 'm_kategori_pemeriksaan',
      required : true
    },
    m_sub_kategori_pemeriksaan_id : {
      model : 'm_sub_kategori_pemeriksaan',
      required : true
    },
    m_pilihan_hasil : {
      collection : 'm_pilihan_hasil',
      via : 'm_item_pemeriksaan_id'
    },
    m_nilai_rujukan : {
      collection : 'm_nilai_rujukan',
      via : 'm_item_pemeriksaan_id'
    },
    m_r_paket_pemeriksaan : {
      collection : 'm_r_paket_pemeriksaan',
      via : 'm_item_pemeriksaan_id'
    },
    m_mapping_tindakan_sim_rs : {
      collection : 'm_mapping_tindakan_sim_rs',
      via : 'm_item_pemeriksaan_id'
    }
  },

};

