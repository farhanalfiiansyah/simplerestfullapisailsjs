/**
 * M_group_flag_text.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    kode : {
      type : 'string'
    },
    m_flag_id_normal : {
      model : 'm_flag',
      required : true
    },
    m_flag_id_tidak_normal : {
      model : 'm_flag',
      required : true
    },
    m_nilai_rujukan : {
      collection : 'm_nilai_rujukan',
      via : 'm_group_flag_text_id'
    }
  },

};

