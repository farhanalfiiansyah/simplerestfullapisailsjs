/**
 * NilaiRujukanController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
    async find(req,res){
        try {
            if (!req.query.page_size || !req.query.page) {
                return res.json({
                    success : false,
                    message : "page_size or page is null"
                })
            }
            const page_size = req.query.page_size;
            const page = req.query.page;
            const data_count = await m_nilai_rujukan.count();
            const skip = page_size * page;
            const data = await m_nilai_rujukan.find({
                limit : page_size,
                skip : skip,
                sort : 'id ASC'
            })
            .populate('m_group_flag_angka_id')
            .populate('m_group_flag_text_id')
            .populate('m_item_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : page_size
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await m_nilai_rujukan.findOne({
                id : req.params.id
            })
            .populate('m_group_flag_angka_id')
            .populate('m_group_flag_text_id')
            .populate('m_item_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async create(req,res){
        try {
            if (req.body.tampilan_nilai_rujukan === undefined) {
                return res.json({
                    success : false,
                    message : "Tampilan nilai rujukan tidak boleh kosong!"
                })
            }
            if (req.body.jenis_rule === undefined) {
                return res.json({
                    success : false,
                    message : "Jenis rule tidak boleh kosong!"
                })
            }
            if (req.body.m_item_pemeriksaan_id === undefined) {
                return res.json({
                    success : false,
                    message : "Item Pemeriksaan tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_atas_tahun === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_atas_tahun tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_atas_bulan === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_atas_bulan tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_atas_hari === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_atas_hari tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_bawah_tahun === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_bawah_tahun tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_bawah_bulan === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_bawah_bulan tidak boleh kosong!"
                })
            }
            if (req.body.rule_batas_umur_bawah_hari === undefined) {
                return res.json({
                    success : false,
                    message : "rule_batas_umur_bawah_hari tidak boleh kosong!"
                })
            }
            if (req.body.rule_jenis_kelamin === undefined) {
                return res.json({
                    success : false,
                    message : "rule_jenis_kelamin tidak boleh kosong!"
                })
            }
            if (req.body.rule_nilai_angka_rujukan_bawah === undefined) {
                return res.json({
                    success : false,
                    message : "rule_nilai_angka_rujukan_bawah tidak boleh kosong!"
                })
            }
            if (req.body.rule_nilai_angka_rujukan_atas === undefined) {
                return res.json({
                    success : false,
                    message : "rule_nilai_angka_rujukan_atas tidak boleh kosong!"
                })
            }
            if (req.body.rule_nilai_angka_kritis_atas === undefined) {
                return res.json({
                    success : false,
                    message : "rule_nilai_angka_kritis_atas tidak boleh kosong!"
                })
            }
            if (req.body.rule_nilai_angka_kritis_bawah === undefined) {
                return res.json({
                    success : false,
                    message : "rule_nilai_angka_kritis_bawah tidak boleh kosong!"
                })
            }
            if (req.body.rule_nilai_text_nilai_rujukan === undefined) {
                return res.json({
                    success : false,
                    message : "rule_nilai_text_nilai_rujukan tidak boleh kosong!"
                })
            }
            const data = await m_nilai_rujukan.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            })
            .populate('m_group_flag_angka_id')
            .populate('m_item_pemeriksaan_id')
            .populate('m_group_flag_text_id');
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_nilai_rujukan.destroy({
                id:req.query.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_nilai_rujukan.updateOne({
                id:req.query.id
            }).set(req.body);
            const newData = await m_nilai_rujukan.findOne({
                id : req.query.id
            })
            .populate('m_group_flag_angka_id')
            .populate('m_item_pemeriksaan_id')
            .populate('m_group_flag_text_id');
            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : newData
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }


};

