/**
 * GroupFlagTextController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    async find(req,res){
        try {
            if (!req.query.page_size || !req.query.page) {
                return res.json({
                    success : false,
                    message : "page_size or page is null"
                })
            }
            const page_size = req.query.page_size;
            const page = req.query.page;
            const data_count = await m_group_flag_text.count();
            const skip = page_size * page;
            const data = await m_group_flag_text.find({
                limit : page_size,
                skip : skip,
                sort : 'id ASC'
            })
            .populate('m_flag_id_normal')
            .populate('m_flag_id_tidak_normal');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : page_size
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await m_group_flag_text.findOne({
                id : req.params.id
            });
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async create(req,res){
        try {
            if (req.body.kode === undefined) {
                return res.json({
                    success : false,
                    message : "Nama tidak boleh kosong!"
                })
            }
            if (req.body.m_flag_id_normal === undefined) {
                return res.json({
                    success : false,
                    message : "m_flag_id_normal tidak boleh kosong!"
                })
            }
            if (req.body.m_flag_id_tidak_normal === undefined) {
                return res.json({
                    success : false,
                    message : "m_flag_id_normal tidak boleh kosong!"
                })
            }
            
            const data = await m_group_flag_text.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_group_flag_text.destroy({
                id:req.query.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_group_flag_text.updateOne({
                id:req.query.id
            }).set(req.body);
            const newData = await m_group_flag_text.findOne({
                id : req.query.id
            })
            .populate('m_flag_id_normal')
            .populate('m_flag_id_tidak_normal');
            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : newData
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }

};

