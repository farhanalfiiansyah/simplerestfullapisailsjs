/**
 * SubKategoriPemeriksaanController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
    async find(req,res){
        try {
            if (!req.query.page_size || !req.query.page) {
                return res.json({
                    success : false,
                    message : "page_size or page is null"
                })
            }
            const page_size = req.query.page_size;
            const page = req.query.page;
            const data_count = await m_sub_kategori_pemeriksaan.count();
            const skip = page_size * page;
            const data = await m_sub_kategori_pemeriksaan.find({
                limit : page_size,
                skip : skip,
                sort : 'id ASC'
            }).populate('m_kategori_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : page_size
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await m_sub_kategori_pemeriksaan.findOne({
                id : req.params.id
            }).populate('m_kategori_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async create(req,res){
        try {
            if (req.body.kode === undefined) {
                return res.json({
                    success : false,
                    message : "kode tidak boleh kosong!"
                })
            }
            if (req.body.nama === undefined) {
                return res.json({
                    success : false,
                    message : "nama tidak boleh kosong!"
                })
            }
            if (req.body.no_urut === undefined) {
                return res.json({
                    success : false,
                    message : "no_urut tidak boleh kosong!"
                })
            }
            if (req.body.status === undefined) {
                return res.json({
                    success : false,
                    message : "status tidak boleh kosong!"
                })
            }
            if (req.body.m_kategori_pemeriksaan_id === undefined) {
                return res.json({
                    success : false,
                    message : "m_kategori_pemeriksaan_id tidak boleh kosong!"
                })
            }
            
            const data = await m_sub_kategori_pemeriksaan.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_sub_kategori_pemeriksaan.destroy({
                id:req.query.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_sub_kategori_pemeriksaan.updateOne({
                id:req.query.id
            }).set(req.body);
            const newData = await m_sub_kategori_pemeriksaan.findOne({
                id : req.query.id
            }).populate('m_kategori_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : newData
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }

};

