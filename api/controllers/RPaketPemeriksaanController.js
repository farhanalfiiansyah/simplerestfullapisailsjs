/**
 * RPaketPemeriksaanController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


module.exports = {
    async find(req,res){
        try {
            if (!req.query.page_size || !req.query.page) {
                return res.json({
                    success : false,
                    message : "page_size or page is null"
                })
            }
            const page_size = req.query.page_size;
            const page = req.query.page;
            const data_count = await m_r_paket_pemeriksaan.count();
            const skip = page_size * page;
            const data = await m_r_paket_pemeriksaan.find({
                limit : page_size,
                skip : skip,
                sort : 'id ASC'
            })
            .populate('m_item_pemeriksaan_id')
            .populate('m_paket_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : page_size
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await m_r_paket_pemeriksaan.findOne({
                id : req.params.id
            })
            .populate('m_item_pemeriksaan_id')
            .populate('m_paket_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async create(req,res){
        try {
            if (req.body.m_item_pemeriksaan_id === undefined) {
                return res.json({
                    success : false,
                    message : "item pemeriksaan tidak boleh kosong!"
                })
            }
            if (req.body.m_paket_pemeriksaan_id === undefined) {
                return res.json({
                    success : false,
                    message : "r paket pemeriksaan tidak boleh kosong!"
                })
            }
            
            const data = await m_r_paket_pemeriksaan.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_r_paket_pemeriksaan.destroy({
                id:req.query.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            if (req.query.id === undefined) {
                return res.json({
                    success : false,
                    message : "id kosong!"
                });
            }
            const data = await m_r_paket_pemeriksaan.updateOne({
                id:req.query.id
            }).set(req.body);
            const newData = await m_r_paket_pemeriksaan.findOne({
                id : req.query.id
            })
            .populate('m_item_pemeriksaan_id')
            .populate('m_paket_pemeriksaan_id');
            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : newData
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }

};

