/**
 * JenisUnitAsalController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    async find(req,res){
        try {
            const page_size = req.query.page_size;
            const page = req.query.page;
            const data_count = await m_jenis_unit_asal.count();
            const skip = page_size * page;
            const data = await m_jenis_unit_asal.find({
                limit : page_size,
                skip : skip,
                sort : 'id DESC'
            });            
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : page_size
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await m_jenis_unit_asal.findOne({
                id : req.params.id
            });
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async create(req,res){
        try {
            const data = await m_jenis_unit_asal.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            const data = await m_jenis_unit_asal.destroy({
                id:req.query.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            const data = await m_jenis_unit_asal.updateOne({
                id:req.query.id
            }).set(req.body);

            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }

};

