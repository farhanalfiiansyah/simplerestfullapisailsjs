/**
 * RegistrasiController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    async find(req,res){
        try {
            const limit = req.query.page_size;
            const page = req.query.page;
            const data_count = await t_registrasi.count();
            const skip = limit * page;
            const data = await t_registrasi.find({
                limit : limit,
                skip : skip
            });
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                properties : {
                    page : page,
                    total : data_count,
                    page_size : limit
                },
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async findOne(req,res){
        try {
            const data = await t_registrasi.findOne({
                id : req.params.id
            });
            return res.json({
                success : true,
                message : "Data berhasil ditampilkan",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : "Data gagal ditampilkan"
            });
        }
    },

    async create(req,res){
        try {
            const data = await t_registrasi.create(req.allParams());
            return res.json({
                success : true,
                message : "Data berhasil disimpan"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async delete(req,res){
        try {
            const data = await t_registrasi.destroy({
                id:req.params.id
            });
            return res.json({
                success : true,
                message : "Data berhasil dihapus"
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    },

    async update(req, res){
        try {
            let value = {};
            const data = await t_registrasi.updateOne({
                id:req.query.id
            }).set(req.body);

            return res.json({
                success : true,
                message : "Data berhasil diupdate",
                payload : data
            });
        } catch (error) {
            return res.json({
                success : false,
                message : error
            });
        }
    }
};

